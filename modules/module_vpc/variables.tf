# -----------------------------------------
# VPC
# -----------------------------------------
variable "aws_vpc_cidr_block" {
  type    = string
  default = "0.0.0.0/0"
}
variable "aws_vpc_enable_dns_support" {
  type    = bool
  default = true
}
variable "aws_vpc_enable_dns_hostnames" {
  type    = bool
  default = false
}
variable "aws_vpc_instance_tenancy" {
  type    = string
  default = "default"
}
variable "aws_vpc_tags" {
  type    = map(any)
  default = {}
}
