# -----------------------------------------
# VPC
# -----------------------------------------
resource "aws_vpc" "vpc" {
  cidr_block           = var.aws_vpc_cidr_block
  enable_dns_support   = var.aws_vpc_enable_dns_support
  enable_dns_hostnames = var.aws_vpc_enable_dns_hostnames
  instance_tenancy     = var.aws_vpc_instance_tenancy

  tags = var.aws_vpc_tags
}
