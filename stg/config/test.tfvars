# -----------------------------------------
# common
# -----------------------------------------
service       = "Rrenkei"
env           = "test02"
environment   = "test"
billing_group = "dalian"
exe_env       = "stg"
csv_path      = "data"
region        = "ap-northeast-1"

# -----------------------------------------
# VPC
# -----------------------------------------

aws_vpc_cidr_block                               = "10.51.0.0/16"
aws_vpc_name                                     = "test-jenkins"
aws_subnet_aws_region                            = "ap-northeast-1"
aws_subnet_public1a_cidr                         = "10.72.10.0/24"
aws_subnet_private1a_cidr                        = "10.72.20.0/24"
aws_subnet_tgw1a_cidr                            = "10.72.30.0/24"
peer_owner_id_mule                               = "494141260463"
aws_vpc_peering_connection_peer_owner_id_test    = "421980585535"
aws_vpc_enable_dns_support                       = true
aws_vpc_enable_dns_hostnames                     = true
aws_vpc_instance_tenancy                         = "default"
aws_flow_log_log_destination                     = "arn:aws:s3:::s3-rrenkei-test01-log"
aws_route_destination_cidr_block                 = "10.68.0.0/16"
local_ipv4_network_cidr                          = "0.0.0.0/0"
remote_ipv4_network_cidr                         = "0.0.0.0/0"
aws_vpc_endpoint_s3_service_name                 = "com.amazonaws.ap-northeast-1.s3"
aws_vpc_endpoint_s3_vpc_endpoint_type            = "Gateway"
aws_vpc_endpoint_cwl_private_dns_enabled         = true
aws_vpc_endpoint_cwl_service_name                = "com.amazonaws.ap-northeast-1.logs"
aws_vpc_endpoint_cwl_vpc_endpoint_type           = "Interface"
aws_vpc_endpoint_ec2_private_dns_enabled         = true
aws_vpc_endpoint_ec2_service_name                = "com.amazonaws.ap-northeast-1.ec2"
aws_vpc_endpoint_ec2_vpc_endpoint_type           = "Interface"
aws_vpc_endpoint_monitoring_private_dns_enabled  = true
aws_vpc_endpoint_monitoring_service_name         = "com.amazonaws.ap-northeast-1.monitoring"
aws_vpc_endpoint_monitoring_vpc_endpoint_type    = "Interface"
aws_vpc_endpoint_ec2messages_private_dns_enabled = true
aws_vpc_endpoint_ec2messages_service_name        = "com.amazonaws.ap-northeast-1.ec2messages"
aws_vpc_endpoint_ec2messages_vpc_endpoint_type   = "Interface"
aws_vpc_endpoint_ssm_private_dns_enabled         = true
aws_vpc_endpoint_ssm_service_name                = "com.amazonaws.ap-northeast-1.ssm"
aws_vpc_endpoint_ssm_vpc_endpoint_type           = "Interface"
aws_vpc_endpoint_ssmmessages_private_dns_enabled = true
aws_vpc_endpoint_ssmmessages_service_name        = "com.amazonaws.ap-northeast-1.ssmmessages"
aws_vpc_endpoint_ssmmessages_vpc_endpoint_type   = "Interface"
aws_flow_log_log_destination_type                = "s3"
aws_flow_log_traffic_type                        = "ALL"
vpc-Rrenkei-test01                               = "vpc-Rrenkei-test01"

# -----------------------------------------
# EC2
# -----------------------------------------
aws_instance_ami                                  = "ami-0c78289803a688617"
aws_instance_instance_type                        = "t3.large"
aws_associate_public_ip_address                   = false
aws_instance_iam_role                             = "role-ec2-Rrenkei-test01-GW"
aws_instance_root_block_device_kms_key_id_ebs     = "arn:aws:kms:ap-northeast-1:421980585535:key/ffa08ed2-f364-4b24-bd5c-5f240511adf1"
aws_ec2_private_ip                                = "10.72.20.10"
aws_instance_instance_initiated_shutdown_behavior = "stop"
aws_instance_disable_api_termination              = true
aws_instance_monitoring                           = false
aws_instance_ebs_optimized                        = true
aws_instance_tenancy                              = "default"
aws_instance_http_endpoint                        = "enabled"
aws_instance_http_tokens                          = "required"
aws_instance_root_block_device_volume_type        = "gp2"
aws_instance_root_block_device_volume_size        = "200"
aws_instance_delete_on_termination                = true
aws_instance_root_block_device_encrypted          = true
aws_ec2_cpu_credits                               = "standard"
StartStopSchedule                                 = "my-office-hours"
sgr-Rrenkei-test02-GW                             = "sgr-Rrenkei-test02-GW"
sub-Rrenkei-test02-private-1a                     = "sub-Rrenkei-test02-private-1a"

# -----------------------------------------
# volume
# -----------------------------------------

volume_size              = 200
volume_availability_zone = "ap-northeast-1a"
device_name              = "/dev/xvda"

# -----------------------------------------
# NLB
# -----------------------------------------
aws_lb_accesslogsbucket                          = "s3-rrenkei-test01-log"
aws_lb_private1a_ipv4_address                    = "10.72.20.80"
aws_lb_internal                                  = true
aws_lb_load_balancer_type                        = "network"
aws_lb_enable_deletion_protection                = true
aws_lb_accesslogs_enabled                        = true
aws_lb_target_group_port                         = 80
aws_lb_target_group_protocol                     = "TCP"
aws_lb_target_group_target_type                  = "instance"
aws_lb_target_group_deregistration_delay         = 350
aws_lb_listener_listener80_port                  = 80
aws_lb_listener_listener80_protocol              = "TCP"
aws_lb_listener_listener80_type                  = "forward"
aws_lb_listener_listener443_port                 = 443
aws_lb_listener_listener443_protocol             = "TLS"
aws_lb_listener_listener443_ssl_policy           = "ELBSecurityPolicy-FS-1-2-Res-2019-08"
aws_lb_listener_listener443_type                 = "forward"
aws_lb_target_group_attachment_tg_attach_1a_port = 80
acm-Rrenkei-test02-01                            = "acm-Rrenkei-test02-01"

# -----------------------------------------
# ACM
# -----------------------------------------
aws_acm_certificate_domain_name               = "*.api-test-kokopass-cpr.com"
aws_acm_certificate_validation_method         = "DNS"
aws_acm_certificate_subject_alternative_names = ["api-test-kokopass-cpr.com"]

# -----------------------------------------
# Backup
# -----------------------------------------
aws_backup_resource_assignment_arn   = "arn:aws:iam::421980585535:role/role-ec2-Rrenkei-test01-Backup"
aws_backup_selection_name            = "bkup-ec2-Rrenkei-test02-Resource"
data_backup_plan_id                  = "682bc6aa-a934-49b4-b05a-fc099542ea7a"
aws_instance_key_name                = "key-Rrenkei-test01"
existing_security_group_bastion_name = "sgr-Rrenkei-test01-Bastion"
existing_security_group_GW_name      = "sgr-Rrenkei-test01-GW"
ec2-Rrenkei-test02-GW-001            = "ec2-Rrenkei-test02-GW-001"

# -----------------------------------------
# CloudWatch
# -----------------------------------------

aws_cloudwatch_metric_alarm_action_arn = "arn:aws:sns:ap-northeast-1:421980585535:%s"
recover_actions_arn                    = "arn:aws:automate:ap-northeast-1:ec2:recover"


# -----------------------------------------
# firehose
# -----------------------------------------
aws_kinesis_firehose_delivery_stream_firehose_arn    = "arn:aws:iam::421980585535:role/role-ec2-Rrenkei-test01-Firehose"
aws_kinesis_firehose_delivery_stream_bucket_arn      = "arn:aws:s3:::s3-rrenkei-test01-log"
aws_kinesis_firehose_delivery_stream_prefix          = "AWSLogs/421980585535/CloudWatchLogs/"
buffer_size                                          = 1
buffer_interval                                      = 300
cloudwatch_logging_options_enabled                   = true
aws_kinesis_firehose_delivery_stream_log_group_name  = "/aws/kinesisfirehose/fds-Rrenkei-test02"
aws_kinesis_firehose_delivery_stream_log_stream_name = "S3Deliverys"
server_side_encryption_enabled                       = false
destination                                          = "extended_s3"

# -----------------------------------------
# route53
# -----------------------------------------
aws_route53_zone_name    = "api-test-kokopass-cpr.com"
route53_name             = "api-test-kokopass-cpr.com"
route53_type             = "A"
evaluate_target_health   = true
nlb-Rrenkei-test02-GW-01 = "nlb-Rrenkei-test02-GW-01"
