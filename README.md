# Terraformを利用したAWS環境構築流れ
## AWSアカウント

- Stepアカウント
  - IAMユーザを作成するアカウント
  - 各環境にIAMロールを作成し、このアカウントからスイッチロールする
- Terraformアカウント
  - tfstateを格納
  - このアカウントからAssumeRoleして各環境にリソースを展開する
  - tfstate格納用S3バケットのアクセスポリシーを厳しくするならStepアカウントで兼用してもOK
  - Terraformのソースコード格納用のCodeCommitや、plan,apply用のCodeBuildもこのアカウントに作成する
- Workloadアカウント
  - アプリをデプロイするアカウント
  - dev, stg, prd, etc..

## tfstate
Terraform自体が利用するリソースをTerraformで管理する事はアンチパターンに当たるため、tfstate格納用のS3バケットについてはマネジメントコンソールやCLIで作成する。

CLIで作成する際のコマンド
```shell
#バケット作成
aws s3api create-bucket \
  --bucket $PROJECT_NAME \
  --acl private \
  --create-bucket-configuration '{"LocationConstraint": "ap-northeast-1"}'

#サーバサイド暗号化を有効化
aws s3api put-bucket-encryption \
  --bucket $PROJECT_NAME \
  --server-side-encryption-configuration '{"Rules":[{"ApplyServerSideEncryptionByDefautl":{"SSEAlgorithm":"AES256"}}]}'

#バージョニングを有効化
aws s3api put-bucket-versioning \
  --bucket $PROJECT_NAME \
  --versioning-configuration '{"Status":"Enabled"}'

#パブリックアクセス無効化
aws s3api put-public-access-block \
  --bucket $PROJECT_NAME \
  --public-access-block-configuration '{"BlockPublicAcls":true,"IgnorePublicAcls":true,"BlockPublicPolicy":true,"RestrictPublicBuckets":true}'
```

## GitLab
共通ModuleをGitLabから取得するために予め開発者をGitLabに招待しておく
- GitLabグループの作成
- GitLabプロジェクトの作成
- GitLabメンバーの追加

## ローカル開発環境
開発環境
- tfenvでTerraformのバージョン管理
  - 各開発者のTerraformのバージョンを固定するため
  - https://github.com/tfutils/tfenv/releases
- エディタはVS Code
  - [Hashicorp Terraform](https://marketplace.visualstudio.com/items?itemName=HashiCorp.terraform)エクステンションを導入している(Language Server   Protocolを利用するため)
  - また`Format On Save`機能を有効化する事でファイルを保存する度に`terraform fmt`を自動で実行し、コードを整形する

# ディレクトリ構成
moduleディレクトリ内にGitLabから取得したソースコードを配置する  
環境毎にディレクトリを作成(terraform workspaceを使用する場合はその限りでない)する  
環境ディレクトリ内でリソースのライフサイクル(レイヤ)毎にディレクトリを分ける  

- セキュリティレイヤ
  - IAM等の権限周り
- 共通レイヤ
  - Route53やSES等、複数のレイヤ,アプリから参照するリソース
- ネットワークレイヤ
  - VPCやサブネット等、一度作成したら滅多に消すことが無いリソース
- DBレイヤ
  - RDS, Elasticache等のデータを格納するリソース
- アプリケーションレイヤ
  - ECS, EC2, ELB等アプリのリリース等で頻繁に変更するリソース

```shell
.
├── modules
│   ├── module_a          <- GitLabからModuleのソースコードを取得して配置する
│   │   ├── main.tf
│   │   ├── outputs.tf
│   │   └── variables.tf
│   └── module_b          <- GitLabからModuleのソースコードを取得して配置する
│       ├── main.tf
│       ├── outputs.tf
│       └── variables.tf
├── dev                   <- 環境毎にディレクトリを作成
│   ├── security          <- レイヤ毎にディレクトリを作成
│   │   ├── main.tf
│   │   └── variables.tf
│   ├── network
│   │   ├── main.tf
│   │   └── variables.tf
│   ├── application
│   │   ├── main.tf
│   │   └── variables.tf
│   └── database
│       ├── main.tf
│       └── variables.tf
︙(環境毎にディレクトリを増やしていく)
```

---

# コーディング規約

## フォーマット
- インデントはスペース2つのソフトタブ
- commit前に`terraform fmt`コマンドを実行して成形する

---

## ファイルの命名規則
- 対応するAWSサービス名
  - `iam.tf`,`lambda.tf`など

---

## 命名規則
- リソース名はスネークケースで記述する
  - 各ProviderのResource Typeの命名規則に合わせる
  - ケバブケース,キャメルケースはNG
- AWS内部のリソース名はケバブケースで記述する
  - AWSマネージドのリソースはローワーキャメルケースが多いので、それと区別するため

---

## Resource
- typeに使用されている文字列を繰り返す事は避ける
- 環境名を入れる事は避ける

```terraform
# NG
# iam_roleという文字列を繰り返し使うのは冗長なので避ける
resource "iam_role" "lambda_iam_role"

# NG
# そもそも環境毎にディレクトリが分かれていたり、workspaceを利用する場合はvariableで環境名を渡すので、名前空間に環境名を入れる必要は無い
resource "iam_role" "lambda_stg"

# OK
# そのIAMロールがどのAWSサービスで使われるのか、あるいは役割が判ればOK
resource "iam_role" "lambda"
resource "iam_role" "developer"
```

## 変数(Variable)
- 環境ごとに異なる値はVariableを宣言し、applyする際にtfvarsファイルで渡す

## 定数(Local Value)
- 定数(繰り返し使用し、全環境で共通している値)はLocals Valueに渡す

---

# コミットメッセージ
prefixをつける

- [add] 
  - resource,dataブロックなどを追加したとき
  - パラメータを追加したとき
- [update]
  - パラメータを変更したとき
- [fix]
  - タイポの修正など
- [mv]
  - ファイルの移動
  - ファイル名変更
- [rm]
  - ファイルの削除

---

# ブランチ戦略
- masterブランチからfeatureやhotfixブランチを切って開発を進める
- コードの追記・変更等が完了次第、masterブランチにマージしていく
- 任意のタイミングでmasterブランチで`terraform apply`を実行

```mermaid
%%{init: { 'theme': 'dark', 'gitGraph': {'mainBranchName': 'main','showCommitLabel': true}} }%%
gitGraph
  commit
  branch feature/foo_1
  commit id: "リソース追加"
  checkout main
  merge feature/foo_1
  branch hotfix/bar_2
  commit id: "バグ修正・パラメータ修正"
  checkout main
  merge hotfix/bar_2
  branch feature/foobar_3
  commit id: "機能追加"
  checkout main
  merge feature/foobar_3
```

## ブランチの命名規則

### prefix
- `feature/`
  - 機能・リソースを追加する時
- `hotfix/`
  - インシデント発生時に暫定的に対応する時
  - インシデントが発生していなくても、パラメータのミスなどに気付いて即修正したい時
- `tmp/`
  - 一時的なリソースを用意したい時
- `fixup/`
  - 不要になったリソースを削除する時

### suffix
- 追跡しやすいよう、チケットのIDなどを付ける
  - GitHubだとIssueの番号をつけるなど
  - 別のチケット管理ツールを使うならそれでもOK

---

# CI/CD
- CodeBuild
  - plan
    - CodeBuildの`Start Build with overrides`を利用し、featureやhotfixブランチ等を参照する
  - apply
    - masterブランチを参照する
  
