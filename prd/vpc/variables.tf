# -----------------------------------------
# common
# -----------------------------------------
variable "billing_group" { type = string }
variable "region" { description = "ap-northeast-1" }

# -----------------------------------------
# VPC
# -----------------------------------------

variable "aws_vpc_cidr_block" { type = string }
variable "aws_vpc_name" { type = string }
variable "aws_vpc_enable_dns_support" { type = bool }
variable "aws_vpc_enable_dns_hostnames" { type = bool }
variable "aws_vpc_instance_tenancy" { type = string }
