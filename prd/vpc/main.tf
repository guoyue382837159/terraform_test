terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.71.0"
    }
  }
}

provider "aws" {
  region = var.region
}

module "vpc" {
  source = "../../modules/module_vpc"
  # -----------------------------------------
  # VPC
  # -----------------------------------------
  aws_vpc_cidr_block           = var.aws_vpc_cidr_block
  aws_vpc_enable_dns_support   = var.aws_vpc_enable_dns_support
  aws_vpc_enable_dns_hostnames = var.aws_vpc_enable_dns_hostnames
  aws_vpc_instance_tenancy     = var.aws_vpc_instance_tenancy
  aws_vpc_tags = {
    "Name"         = var.aws_vpc_name
    "BillingGroup" = var.billing_group
  }
}
