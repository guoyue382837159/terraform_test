# -----------------------------------------
# common
# -----------------------------------------
service       = "Rrenkei"
env           = "prd02"
exe_env       = "prd"
environment   = "production"
billing_group = "dalian"
region        = "ap-northeast-1"
csv_path      = "data"

# -----------------------------------------
# VPC
# -----------------------------------------

aws_vpc_cidr_block                               = "10.50.0.0/16"
aws_vpc_name                                     = "prd_jenkins"
aws_subnet_aws_region                            = "ap-northeast-1"
aws_subnet_public1a_cidr                         = "10.71.10.0/24"
aws_subnet_public1c_cidr                         = "10.71.20.0/24"
aws_subnet_private1a_cidr                        = "10.71.30.0/24"
aws_subnet_private1c_cidr                        = "10.71.40.0/24"
aws_subnet_lifekeeper1a_cidr                     = "10.71.50.0/24"
aws_subnet_lifekeeper1c_cidr                     = "10.71.60.0/24"
aws_subnet_tgw1a_cidr                            = "10.71.70.0/24"
aws_subnet_tgw1c_cidr                            = "10.71.80.0/24"
aws_route_destination_cidr_block                 = "10.69.0.0/16"
aws_flow_log_log_destination                     = "arn:aws:s3:::s3-rrenkei-prd01-log"
aws_vpc_peering_connection_peer_owner_id_test    = "488675835538"
aws_vpc_enable_dns_support                       = true
aws_vpc_enable_dns_hostnames                     = true
aws_vpc_instance_tenancy                         = "default"
aws_vpc_endpoint_s3_service_name                 = "com.amazonaws.ap-northeast-1.s3"
aws_vpc_endpoint_s3_vpc_endpoint_type            = "Gateway"
aws_vpc_endpoint_cwl_private_dns_enabled         = true
aws_vpc_endpoint_cwl_service_name                = "com.amazonaws.ap-northeast-1.logs"
aws_vpc_endpoint_cwl_vpc_endpoint_type           = "Interface"
aws_vpc_endpoint_ec2_private_dns_enabled         = true
aws_vpc_endpoint_ec2_service_name                = "com.amazonaws.ap-northeast-1.ec2"
aws_vpc_endpoint_ec2_vpc_endpoint_type           = "Interface"
aws_vpc_endpoint_monitoring_private_dns_enabled  = true
aws_vpc_endpoint_monitoring_service_name         = "com.amazonaws.ap-northeast-1.monitoring"
aws_vpc_endpoint_monitoring_vpc_endpoint_type    = "Interface"
aws_vpc_endpoint_ec2messages_private_dns_enabled = true
aws_vpc_endpoint_ec2messages_service_name        = "com.amazonaws.ap-northeast-1.ec2messages"
aws_vpc_endpoint_ec2messages_vpc_endpoint_type   = "Interface"
aws_vpc_endpoint_ssm_private_dns_enabled         = true
aws_vpc_endpoint_ssm_service_name                = "com.amazonaws.ap-northeast-1.ssm"
aws_vpc_endpoint_ssm_vpc_endpoint_type           = "Interface"
aws_vpc_endpoint_ssmmessages_private_dns_enabled = true
aws_vpc_endpoint_ssmmessages_service_name        = "com.amazonaws.ap-northeast-1.ssmmessages"
aws_vpc_endpoint_ssmmessages_vpc_endpoint_type   = "Interface"
aws_flow_log_log_destination_type                = "s3"
aws_flow_log_traffic_type                        = "ALL"
vpc-Rrenkei-prd01                                = "vpc-Rrenkei-prd01"

# -----------------------------------------
# EC2
# -----------------------------------------
aws_network_interface_source_dest_check           = false
aws_instance_ami                                  = "ami-04792f7f7ae166636"
aws_instance_instance_type                        = "r5.large"
aws_instance_iam_role                             = "role-ec2-Rrenkei-prd01-GW"
aws_instance_root_block_device_kms_key_id_ebs     = "arn:aws:kms:ap-northeast-1:488675835538:key/b39d2ca7-cd39-4a5a-a1fa-e58abb4d7ce4"
aws_instance_instance_initiated_shutdown_behavior = "stop"
aws_instance_disable_api_termination              = true
aws_instance_monitoring                           = false
aws_instance_ebs_optimized                        = true
aws_instance_tenancy                              = "default"
aws_instance_http_endpoint                        = "enabled"
aws_instance_http_tokens                          = "optional"
aws_instance_root_block_device_volume_type        = "gp2"
aws_instance_root_block_device_volume_size        = "200"
aws_instanc_delete_on_termination                 = true
aws_instance_root_block_device_encrypted          = true
aws_instance_GW_1a_eth0_device_index              = 0
aws_instance_GW_1a_eth1_device_index              = 1
aws_instance_GW_1c_eth0_device_index              = 0
aws_instance_GW_1c_eth1_device_index              = 1
aws_network_interface_private1a_ip                = ["10.71.30.10"]
aws_network_interface_private1c_ip                = ["10.71.40.10"]
aws_network_interface_lifekeeper1a_ip             = ["10.71.50.10"]
aws_network_interface_lifekeeper1c_ip             = ["10.71.60.10"]
aws_network_interface_private1a_description       = "GW-001 eth0"
aws_network_interface_private1c_description       = "GW-002 eth0"
aws_network_interface_lifekeeper1a_description    = "GW-001 eth1"
aws_network_interface_lifekeeper1c_description    = "GW-002 eth1"
sgr-Rrenkei-prd02-GW                              = "sgr-Rrenkei-prd02-GW"
sub-Rrenkei-prd02-private-1a                      = "sub-Rrenkei-prd02-private-1a"
sub-Rrenkei-prd02-private-1c                      = "sub-Rrenkei-prd02-private-1c"
sub-Rrenkei-prd02-lifekeeper-1a                   = "sub-Rrenkei-prd02-lifekeeper-1a"
sub-Rrenkei-prd02-lifekeeper-1c                   = "sub-Rrenkei-prd02-lifekeeper-1c"

# -----------------------------------------
# EFS
# -----------------------------------------
aws_efs_file_system_kms_key_id_efs = "arn:aws:kms:ap-northeast-1:488675835538:key/bc072eb7-283a-4304-8bae-08cef6c85834"
performance_mode                   = "generalPurpose"
throughput_mode                    = "bursting"
encrypted                          = "true"
sgr-Rrenkei-prd02-EFS              = "sgr-Rrenkei-prd02-EFS"

# -----------------------------------------
# volume
# -----------------------------------------

volume_availability_zone = "ap-northeast-1a"
device_name              = "/dev/xvda"

# -----------------------------------------
# NLB
# -----------------------------------------
aws_lb_internal                                  = true
aws_lb_load_balancer_type                        = "network"
aws_lb_enable_cross_zone_load_balancing          = true
aws_lb_enable_deletion_protection                = true
aws_lb_accesslogsbucket                          = "s3-rrenkei-prd01-log"
aws_lb_private1a_ipv4_address                    = "10.71.30.80"
aws_lb_private1c_ipv4_address                    = "10.71.40.80"
aws_lb_accesslogs_enabled                        = true
aws_lb_target_group_port                         = 80
aws_lb_target_group_protocol                     = "TCP"
aws_lb_target_group_target_type                  = "instance"
aws_lb_target_group_deregistration_delay         = 350
aws_lb_listener_listener80_port                  = 80
aws_lb_listener_listener80_protocol              = "TCP"
aws_lb_listener_listener80_type                  = "forward"
aws_lb_listener_listener443_port                 = "443"
aws_lb_listener_listener443_protocol             = "TLS"
aws_lb_listener_listener443_ssl_policy           = "ELBSecurityPolicy-FS-1-2-Res-2019-08"
aws_lb_listener_listener443_type                 = "forward"
aws_lb_target_group_attachment_tg_attach_1a_port = 80
aws_lb_target_group_attachment_tg_attach_1c_port = 80
acm-Rrenkei-prd02-01                             = "acm-Rrenkei-prd02-01"

# -----------------------------------------
# ACM
# -----------------------------------------
aws_acm_certificate_domain_name               = "*.api-kokopass-cpr.com"
aws_acm_certificate_validation_method         = "DNS"
aws_acm_certificate_subject_alternative_names = ["api-kokopass-cpr.com"]

# -----------------------------------------
# Backup
# -----------------------------------------
aws_backup_selection_assignment_arn = "arn:aws:iam::488675835538:role/role-ec2-Rrenkei-prd01-Backup"
aws_backup_selection_name           = "bkup-ec2-Rrenkei-prd02-Resource"

data_backup_plan_id                  = "89839892-7aa4-4d5e-87d1-b8515dd95d60"
aws_instance_key_name                = "key-Rrenkei-prd01"
existing_security_group_bastion_name = "sgr-Rrenkei-prd01-Bastion"
# existing_security_group_GW_name      = "sgr-Rrenkei-prd01-GW"
existing_security_group_mgt_name = "sgr-Rrenkei-prd01-mgt"
prd01_private_1a                 = "sub-Rrenkei-prd01-private-1a"
prd01_private_1c                 = "sub-Rrenkei-prd01-private-1c"
ec2-Rrenkei-prd02-GW-001         = "ec2-Rrenkei-prd02-GW-001"
ec2-Rrenkei-prd02-GW-002         = "ec2-Rrenkei-prd02-GW-002"
efs-Rrenkei-prd02                = "efs-Rrenkei-prd02"


# -----------------------------------------
# CloudWatch
# -----------------------------------------

aws_cloudwatch_metric_alarm_action_arn = "arn:aws:sns:ap-northeast-1:488675835538:%s"
recover_actions_arn                    = "arn:aws:automate:ap-northeast-1:ec2:recover"


# -----------------------------------------
# firehose
# -----------------------------------------
aws_kinesis_firehose_delivery_stream_firehose_arn    = "arn:aws:iam::488675835538:role/role-ec2-Rrenkei-prd01-Firehose"
aws_kinesis_firehose_delivery_stream_bucket_arn      = "arn:aws:s3:::s3-rrenkei-prd01-log"
aws_kinesis_firehose_delivery_stream_prefix          = "AWSLogs/488675835538/CloudWatchLogs/"
aws_kinesis_firehose_delivery_stream_log_group_name  = "/aws/kinesisfirehose/fds-Rrenkei-prd02"
aws_kinesis_firehose_delivery_stream_log_stream_name = "S3Deliverys"
destination                                          = "extended_s3"
buffer_size                                          = 1
buffer_interval                                      = 300
cloudwatch_logging_options_enabled                   = true
server_side_encryption_enabled                       = false

# -----------------------------------------
# route53
# -----------------------------------------
aws_route53_zone_name   = "api-kokopass-cpr.com"
route53_name            = "api-kokopass-cpr.com"
route53_type            = "A"
evaluate_target_health  = false
nlb-Rrenkei-prd02-GW-01 = "nlb-Rrenkei-prd02-GW-01"

# -----------------------------------------
# transit_gateway
# -----------------------------------------
amazon_side_asn                                       = 64521
vpn_ecmp_support                                      = "disable"
default_route_table_association                       = "disable"
default_route_table_propagation                       = "disable"
prd02_transit_gateway_default_route_table_association = false
prd02_transit_gateway_default_route_table_propagation = false
prd01_transit_gateway_default_route_table_association = false
prd01_transit_gateway_default_route_table_propagation = false
kokpass_prd02_destination_cidr_block                  = "10.71.0.0/16"
reps_prd02_destination_cidr_block                     = "10.71.0.0/16"
reps_ftp_destination_cidr_block                       = "10.73.0.10/32"
reps_prd01_destination_cidr_block                     = "10.69.0.0/16"
sub-Rrenkei-prd02-tgw-1a                              = "sub-Rrenkei-prd02-tgw-1a"
sub-Rrenkei-prd02-tgw-1c                              = "sub-Rrenkei-prd02-tgw-1c"
